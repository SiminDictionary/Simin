#!/bin/bash
SIMINP=/opt/simin
USRSIMIN=/usr/bin/simin
sudo mkdir $SIMINP;
sudo cp -r * $SIMINP/
sudo cp simin /usr/bin/
sudo chmod +x $USRSIMIN
sudo cp simin-no /usr/bin/
sudo chmod +x /usr/bin/simin-no
sudo cp simin.desktop /usr/share/applications
sudo rm -rf $SIMINP/simin.desktop $SIMINP/install.sh __pycache__ $SIMINP/simin $SIMINP/simin-no
sudo chown -R $(id -u):$(id -g) $SIMINP
if ! [ -x "$(command -v xclip)" ]; then
  echo 'Error: xclip is not installed.' >&2
  echo 'Installing Xclip'

  if [ -x "$(command -v apt)" ];then
    sudo apt install xclip
  elif [ -x "$(command -v apt-get)" ]; then
    sudo apt-get install xclip
  elif [ -x "$(command -v pacman)" ]; then
    sudo pacman -S xclip
  elif [ -x "$(command -v dnf)" ]; then
    sudo dnf install xclip
  elif [ -x "$(command -v zypper)"]; then
    sudo zypper install xclip
  else
    echo 'Failed to install "xclip" package...'
  fi
fi
